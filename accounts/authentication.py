import requests
from django.contrib.auth import get_user_model
from django.conf import settings
User = get_user_model()
import logging

PERSONA_VERIFY_URL = 'https://verifier.login.persona.org/verify'

logger = logging.getLogger(__name__)
class PersonaAuthenticationBackend():

	def authenticate(self, assertion):
		response = requests.post(
			PERSONA_VERIFY_URL,
			data={'assertion': assertion, 'audience': settings.DOMAIN}
		)

		if response.ok and response.json()['status'] == 'okay':
			try:
				return User.objects.get(email=response.json()['email'])
			except User.DoesNotExist:
				return User.objects.create(email=response.json()['email'])
		else:
			logger.warning('Persona says no: JSON: {}'.format(response.json()))

	def get_user(self, email):
		try:
			return User.objects.get(email=email)
		except User.DoesNotExist:
			return None
