from django.test import TestCase
from django.contrib.auth import get_user_model

User = get_user_model()

class UserModelTest(TestCase):

	def test_only_email_is_required(self):
		user = User(email='a@b.com')
		user.full_clean() # Should not raise

	def test_email_is_primary_key(self):
		user = User(email='a@b.com')
		self.assertFalse(hasattr(user, 'id'))

	def test_user_is_authenticated(self):
		user = User(email='a@b.com')
		self.assertTrue(user.is_authenticated())