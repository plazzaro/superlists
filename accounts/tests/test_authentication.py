from unittest.mock import patch
from django.test import TestCase
from django.contrib.auth import get_user_model
from django.conf import settings

from accounts.authentication import (
	PERSONA_VERIFY_URL, PersonaAuthenticationBackend
)

import logging

User = get_user_model()

@patch('accounts.authentication.requests.post')
class AuthenticationTest(TestCase):

	def setUp(self):
		user = User(email='mary@example.com')
		user.username = 'mary'
		user.save()

		self.backend = PersonaAuthenticationBackend()

	def test_sends_assertion_to_mozilla_with_domain(self, mock_post):

		self.backend.authenticate('an assertion')

		mock_post.assert_called_once_with(
			PERSONA_VERIFY_URL, data={'assertion': 'an assertion', 'audience': settings.DOMAIN}
		)

	def test_returns_none_if_response_is_error(self, mock_post):
		mock_post.return_value.ok = False
		mock_post.return_value.json.return_value = {}
		user = self.backend.authenticate('an assertion')

		self.assertIsNone(user)

	def test_returns_none_if_status_not_okay(self, mock_post):
		mock_post.return_value.json.return_value = {'status': 'not okay!'}

		user = self.backend.authenticate('an assertion')
		self.assertIsNone(user)

	def test_returns_existing_user_with_email(self, mock_post):
		mock_post.return_value.json.return_value = {'status': 'okay', 'email': 'a@b.com'}

		actual_user = User.objects.create(email='a@b.com')

		user = self.backend.authenticate('an assertion')

		self.assertEqual(user, actual_user)

	def test_creates_user_it_one_doesnt_exist_on_valid_assertion(self, mock_post):
		mock_post.return_value.json.return_value = {'status': 'okay', 'email': 'a@b.com'}

		found_user = self.backend.authenticate('an assertion')
		new_user = User.objects.get(email='a@b.com')
		self.assertEqual(new_user, found_user)

	def test_logs_non_okay_responses_from_persona(self, mock_post):
		json_response = {'status': 'not okay', 'reason': 'audience mismatch'}

		mock_post.return_value.json.return_value = json_response
		mock_post.return_value.ok = True

		logger = logging.getLogger('accounts.authentication')

		with patch.object(logger, 'warning') as mock_logger:
			self.backend.authenticate('an assertion')

		mock_logger.assert_called_once_with('Persona says no: JSON: {}'.format(json_response))

class GetUserTest(TestCase):

	def test_get_user_by_email(self):
		backend = PersonaAuthenticationBackend()

		other_user = User(email='mary@example.com')
		other_user.username = 'mary'
		other_user.save()

		desired_user = User.objects.create(email='joe@example.com')

		found_user = backend.get_user('joe@example.com')

		self.assertEqual(desired_user, found_user)

	def test_get_none_when_user_doesnt_exist(self):
		backend = PersonaAuthenticationBackend()

		found_user = backend.get_user('a@b.com')

		self.assertEqual(found_user, None)
