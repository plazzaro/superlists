from django.test import TestCase
from lists.models import List, Item
from lists.forms import (ItemForm, EMPTY_LIST_ERROR,
	DUPLICATE_ITEM_ERROR, ExistingListItemForm, NewListForm)

from unittest.mock import patch, Mock

class ItemFormTest(TestCase):

	def test_form_renders_item_text_input(self):
		form = ItemForm()
		self.assertIn('placeholder="Enter a to-do item"', form.as_p())
		self.assertIn('class="form-control input-lg"', form.as_p())

	def test_form_validation_for_blank_items(self):
		form = ItemForm(data={'text': ''})
		self.assertFalse(form.is_valid())
		self.assertEqual(
			form.errors['text'],
			[EMPTY_LIST_ERROR]
		)

class ExistingListItemFormTest(TestCase):

	def test_form_renders_item_text_input(self):
		list_ = List.objects.create()
		form = ExistingListItemForm(for_list=list_)
		self.assertIn('placeholder="Enter a to-do item"', form.as_p())
		self.assertIn('class="form-control input-lg"', form.as_p())

	def test_form_validation_for_blank_items(self):
		list_ = List.objects.create()
		form = ExistingListItemForm(for_list=list_, data={'text': ''})
		self.assertFalse(form.is_valid())
		self.assertEqual(
			form.errors['text'],
			[EMPTY_LIST_ERROR]
		)

	def test_form_validation_for_duplicate_items(self):
		list_ = List.objects.create()
		Item.objects.create(list=list_, text='Item')

		form = ExistingListItemForm(for_list=list_, data={'text': 'Item'})
		self.assertFalse(form.is_valid())
		self.assertEqual(
			form.errors['text'],
			[DUPLICATE_ITEM_ERROR]
		)

	def test_form_save(self):
		list_ = List.objects.create()
		form = ExistingListItemForm(for_list=list_, data={'text': 'Item'})
		new_item = form.save()
		self.assertEqual(new_item, Item.objects.all()[0])

@patch('lists.forms.List.create_new')
class NewListFormTest(TestCase):

	def test_creates_new_list_from_POST_data_if_user_isnt_authenticated(self,
			mock_create_new):

		user = Mock(is_authenticated=lambda: False)

		form = NewListForm(data={'text': 'A new list item'})
		form.is_valid()
		form.save(owner=user)

		mock_create_new.assert_called_once_with(first_item_text='A new list item')

	def test_creates_new_list_from_POST_data_if_user_isnt_authenticated(self,
			mock_create_new):

		user = Mock(is_authenticated=lambda: True)

		form = NewListForm(data={'text': 'A new list item'})
		form.is_valid()
		form.save(owner=user)

		mock_create_new.assert_called_once_with(first_item_text='A new list item', owner=user)

	def test_creates_new_list_and_returns_list_object(self, mock_create_new):

		user = Mock(is_authenticated=lambda: True)

		form = NewListForm(data={'text': 'a new list item'})
		form.is_valid()
		list_ = form.save(owner=user)

		self.assertEqual(list_, mock_create_new.return_value)
