from django.core.urlresolvers import resolve
from django.template.loader import render_to_string
from django.utils.html import escape
from django.test import TestCase
from django.http import HttpRequest
from django.contrib.auth import get_user_model

from lists.models import Item, List
from lists.forms import (
    ItemForm, EMPTY_LIST_ERROR,
    DUPLICATE_ITEM_ERROR, ExistingListItemForm
    )
from lists.views import new_list

from unittest import skip
from unittest.mock import patch, Mock

User = get_user_model()


class HomePageTest(TestCase):

    def test_home_page_renders_home_template(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'home.html')

    def test_home_page_uses_item_form(self):
        response = self.client.get('/')
        self.assertIsInstance(response.context['form'], ItemForm)


class NewListIntegratedTest(TestCase):

    def test_saving_a_POST_request(self):
        self.client.post(
            '/lists/new',
            data={'text': 'A new list item'})

        self.assertEqual(Item.objects.count(), 1)
        new_item = Item.objects.first()
        self.assertEqual(new_item.text, 'A new list item')

    def test_list_owner_is_set_if_user_is_authenticated(self):
        request = HttpRequest()
        request.user = User.objects.create(email='a@b.com')

        request.POST['text'] = 'An Item'

        new_list(request)

        list_ = List.objects.first()
        self.assertEqual(list_.owner, request.user)

    def test_invalid_list_items_arent_saved(self):
        self.client.post('/lists/new', data={'text': ''})
        self.assertEqual(List.objects.count(), 0)
        self.assertEqual(Item.objects.count(), 0)


class ListViewTest(TestCase):

    def post_invalid_input(self):
        list_ = List.objects.create()
        return self.client.post('/lists/%d/' % (list_.id), data={'text': ''})

    def test_uses_the_list_template(self):
        list_ = List.objects.create()
        response = self.client.get('/lists/%d/' % (list_.id))
        self.assertTemplateUsed(response, 'list.html')

    def test_passes_the_correct_list_to_the_template(self):
        other_list = List.objects.create()
        correct_list = List.objects.create()

        response = self.client.get('/lists/%d/' % (correct_list.id))

        self.assertEqual(response.context['list'], correct_list)

    def test_displays_only_items_for_the_list(self):
        correct_list = List.objects.create()
        Item.objects.create(text='Item 1', list=correct_list)
        Item.objects.create(text='Item 2', list=correct_list)
        other_list = List.objects.create()
        Item.objects.create(text='Other Item 1', list=other_list)
        Item.objects.create(text='Other Item 2', list=other_list)
        response = self.client.get('/lists/%d/' % (correct_list.id))

        self.assertContains(response, 'Item 1')
        self.assertContains(response, 'Item 2')
        self.assertNotContains(response, 'Other Item 1')
        self.assertNotContains(response, 'Other Item 2')

    def test_can_save_a_POST_to_an_existing_list(self):
        other_list = List.objects.create()
        correct_list = List.objects.create()

        self.client.post(
            '/lists/%d/' % (correct_list.id),
            data={'text': 'A new item for an existing list'}
        )

        self.assertEqual(Item.objects.count(), 1)
        new_item = Item.objects.first()
        self.assertEqual(new_item.text, 'A new item for an existing list')
        self.assertEqual(new_item.list, correct_list)

    def test_POST_redirects_to_list(self):
        other_list = List.objects.create()
        correct_list = List.objects.create()

        response = self.client.post(
            '/lists/%d/' % (correct_list.id),
            data={'text': 'A new item for an existing list'}
        )

        self.assertRedirects(response, 'lists/%d/' % (correct_list.id))

    def test_displays_item_form(self):
        list_ = List.objects.create()
        response = self.client.get('/lists/%d/' % (list_.id))

        self.assertIsInstance(response.context['form'], ExistingListItemForm)
        self.assertContains(response, 'name="text"')

    def test_for_invalid_input_nothing_saved(self):
        self.post_invalid_input()
        self.assertEqual(Item.objects.count(), 0)

    def test_for_invalid_input_renders_list_template(self):
        response = self.post_invalid_input()
        self.assertTemplateUsed(response, 'list.html')

    def test_for_invalid_input_displays_item_form(self):
        response = self.post_invalid_input()
        self.assertIsInstance(response.context['form'], ExistingListItemForm)

    def test_for_invalid_input_shows_error_on_page(self):
        response = self.post_invalid_input()
        self.assertContains(response, escape(EMPTY_LIST_ERROR))

    def test_duplicate_item_validation_errors_show_on_list_page(self):
        list_ = List.objects.create()
        item1 = Item.objects.create(list=list_, text='Item 1')

        response = self.client.post(
            '/lists/%d/' % list_.id,
            data={'text': 'Item 1'}
        )

        expected_error = escape(DUPLICATE_ITEM_ERROR)
        self.assertContains(response, expected_error)
        self.assertTemplateUsed(response, 'list.html')
        self.assertEqual(Item.objects.count(), 1)


@patch('lists.views.NewListForm')
class NewListTest(TestCase):

    def setUp(self):
        self.request = HttpRequest()
        self.request.POST['text'] = 'An Item'
        self.request.user = Mock()

    def test_passes_POST_data_to_NewListForm(self, mockNewListForm):
        new_list(self.request)
        mockNewListForm.assert_called_once_with(data=self.request.POST)

    def test_saves_form_with_owner_if_valid(self, mockNewListForm):
        mock_form = mockNewListForm.return_value
        mock_form.is_valid.return_value = True
        new_list(self.request)

        mock_form.save.assert_called_once_with(owner=self.request.user)

    @patch('lists.views.redirect')
    def test_redirects_to_form_returned_object_when_form_is_valid(
            self, mock_redirect, mockNewListForm):
        mock_form = mockNewListForm.return_value
        mock_form.is_valid.return_value = True

        response = new_list(self.request)

        self.assertEqual(response, mock_redirect.return_value)
        mock_redirect.assert_called_once_with(mock_form.save.return_value)

    @patch('lists.views.render')
    def test_renders_the_homepage_if_form_is_not_valid(
            self,  mock_render, mockNewListForm):
        mock_form = mockNewListForm.return_value
        mock_form.is_valid.return_value = False

        response = new_list(self.request)

        self.assertEqual(response, mock_render.return_value)
        mock_render.assert_called_once_with(
            self.request, 'home.html', {'form': mock_form}
        )

    def test_does_not_save_if_not_valid(self, mockNewListForm):
        mock_form = mockNewListForm.return_value
        mock_form.is_valid.return_value = False

        new_list(self.request)

        self.assertFalse(mock_form.save.called)


class MyListsTest(TestCase):

    def test_my_lists_url_renders_correct_template(self):
        User.objects.create(email='a@b.com')
        response = self.client.get('/lists/user/a@b.com/')
        self.assertTemplateUsed(response, 'my_lists.html')

    def test_correct_user_gets_passed_to_template(self):
        wrong_user = User.objects.create(email='wrong@example.com')
        correct_user = User.objects.create(email='correct@example.com')
        response = self.client.get('/lists/user/correct@example.com/')

        self.assertEqual(response.context['owner'], correct_user)

    def test_shared_lists_are_passed_to_template(self):
        user = User.objects.create(email='oni@example.com')
        list_ = List.objects.create()
        list_.shared_with.add(user)

        response = self.client.get('/lists/user/oni@example.com/')

        self.assertIn(list_, response.context['shared_lists'])

    def test_only_lists_shared_with_use_are_in_shared_with(self):

        user = User.objects.create(email='oni@example.com')
        list1 = List.objects.create()
        list2 = List.objects.create()
        list1.shared_with.add(user)
        response = self.client.get('/lists/user/oni@example.com/')

        self.assertNotIn(list2, response.context['shared_lists'])


class ShareListsTest(TestCase):

    def test_POST_redirects_back_to_list(self):
        list_ = List.objects.create()
        user = User.objects.create(email='oni@example.com')

        response = self.client.post(
            '/lists/%d/share' % list_.id,
            data={'email': 'oni@example.com'}
        )
        self.assertRedirects(response, 'lists/%d/' % list_.id)

    def test_user_is_created_if_doesnt_exist(self):
        list_ = List.objects.create()
        response = self.client.post(
            '/lists/%d/share' % list_.id,
            data={'email': 'oni@example.com'}
        )

        self.assertEqual(User.objects.first().email, 'oni@example.com')

    def test_user_gets_added_to_shared_with(self):
        list_ = List.objects.create()
        user = User.objects.create(email='oni@example.com')

        self.client.post(
            '/lists/%d/share' % list_.id,
            data={'email': 'oni@example.com'}
        )

        self.assertIn(user, list_.shared_with.all())

    def test_shared_with_gets_passed_to_the_template(self):
        list_ = List.objects.create()
        user = User.objects.create(email='oni@example.com')

        list_.shared_with.add(user)

        response = self.client.get(
            '/lists/%d/' % list_.id
        )

        self.assertContains(response, 'oni@example.com')
