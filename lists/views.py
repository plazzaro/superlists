from django.core.exceptions import ValidationError, ObjectDoesNotExist
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth import get_user_model


from lists.models import Item, List
from lists.forms import ItemForm, ExistingListItemForm, NewListForm

User = get_user_model()


def home_page(request):
    return render(request, 'home.html', {'form': ItemForm()})


def view_list(request, list_id):
    list_ = List.objects.get(id=list_id)

    form = ExistingListItemForm(for_list=list_)
    if request.method == 'POST':
        form = ExistingListItemForm(
            for_list=list_, data=request.POST
        )
        if form.is_valid():
            form.save()
            return redirect(list_)
    return render(request, 'list.html', {'list': list_, 'form': form})


def new_list(request):
    form = NewListForm(data=request.POST)

    if form.is_valid():
        list_ = form.save(owner=request.user)
        return redirect(list_)
    else:
        return render(request, 'home.html', {'form': form})


def my_lists(request, email):
    owner = User.objects.get(email=email)
    shared_lists = List.objects.filter(shared_with=owner)
    return render(
        request,
        'my_lists.html',
        {'owner': owner, 'shared_lists': shared_lists}
    )


def share_list(request, list_id):
    list_ = List.objects.get(id=list_id)
    if request.method == 'POST':
        try:
            user = User.objects.get(email=request.POST['email'])
            list_.shared_with.add(user)
        except ObjectDoesNotExist:
            list_.shared_with.create(email=request.POST['email'])
    return redirect(list_)
