from .base import FunctionalTest

class MyListsTest(FunctionalTest):

	def test_logged_in_users_lists_are_saved_as_my_lists(self):
		email = 'edith@example.com'

		# Edith is a logged in user
		self.create_pre_authenticated_session(email)

		self.browser.get(self.server_url)
		self.wait_to_be_logged_in(email)

		# She creates a new list
		self.get_item_input_box().send_keys('Item 1\n')
		self.get_item_input_box().send_keys('Item 2\n')

		first_list_url = self.browser.current_url

		# She notices a new link named 'My Lists' and decides to check it out
		self.browser.find_element_by_link_text('My Lists').click()

		# She sees that her list is there, with a name of the first item and clicks it
		self.browser.find_element_by_link_text('Item 1').click()

		# This takes her to that list page
		self.wait_for(
			lambda: self.assertEqual(first_list_url, self.browser.current_url)
		)


		# She decides to start a new list just to check
		self.browser.get(self.server_url)
		self.get_item_input_box().send_keys('New list item\n')
		second_list_url = self.browser.current_url

		# She clicks on my lists again and now the new list there
		self.browser.find_element_by_link_text('My Lists').click()
		self.browser.find_element_by_link_text('New list item').click()

		self.wait_for(
			lambda: self.assertEqual(second_list_url, self.browser.current_url)
		)

		# She logs out and is happy she can longer access her lists
		self.browser.find_element_by_id('id_logout').click()
		self.assertEqual(
			self.browser.find_elements_by_link_text('My Lists'),
			[]
		)
