from .base import FunctionalTest
from selenium.webdriver.support.ui import WebDriverWait
import time

TEST_EMAIL='edith@mockmyid.com'

class LoginTest(FunctionalTest):

	def switch_to_new_window(self, text_in_title):
		retries = 50
		while retries > 0:
			for handle in self.browser.window_handles:
				self.browser.switch_to_window(handle)
				if text_in_title in self.browser.title:
					return
			retries -= 1
			time.sleep(0.5)
		self.fail('Could not find window')

	def test_login_with_persona(self):
		# Edith goes to the superlists site and notices a Sign-In link
		# and clicks it
		self.browser.get(self.server_url)
		self.browser.find_element_by_id('id_login').click()

		# A Persona login window appears
		self.switch_to_new_window('Mozilla Persona')

		# Edith signs in with her email
		## Use mockmyid.com for test email
		self.browser.find_element_by_id(
			'authentication_email'
		).send_keys(TEST_EMAIL)

		self.browser.find_element_by_tag_name('button').click()

		# The Persona window closes
		self.switch_to_new_window('To-Do')

		# She can see that she is logged in
		self.wait_to_be_logged_in(TEST_EMAIL)

		# After refreshing the page she can see that she is still logged in.
		self.browser.refresh()
		self.wait_to_be_logged_in(TEST_EMAIL)

		# She then logs out
		self.browser.find_element_by_id('id_logout').click()
		self.wait_to_be_logged_out(TEST_EMAIL)

		# she refreshes and is still logged out
		self.browser.refresh()
		self.wait_to_be_logged_out(TEST_EMAIL)
