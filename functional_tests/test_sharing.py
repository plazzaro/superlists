from selenium import webdriver
from .base import FunctionalTest
from .home_and_list_pages import HomePage, ListPage


def quit_if_posible(browser):
    try:
        browser.quit()
    except:
        pass


class SharingTest(FunctionalTest):

    def test_logged_in_users_lists_are_saved_as_my_lists(self):
        # Edith is a logged in user
        self.create_pre_authenticated_session('edith@example.com')
        edith_browser = self.browser
        self.addCleanup(lambda: quit_if_posible(edith_browser))

        # Her friend Oni is also on the website
        oni_browser = webdriver.Firefox()
        self.addCleanup(lambda: quit_if_posible(oni_browser))
        self.browser = oni_browser
        self.create_pre_authenticated_session('oni@example.com')

        # Edith goes to the homepage and creates a list
        self.browser = edith_browser
        list_page = HomePage(self).start_new_list('Edith Item 1')

        # She notices a share this list option

        share_box = list_page.get_share_box()

        self.assertEqual(
            share_box.get_attribute('placeholder'),
            'your-friend@example.com'
        )

        # She shares a list with with Oni
        # The page updates to show her that the list has been shared
        list_page.share_list_with('oni@example.com')

        # Oni now goes to his My Lists page and sees Edith's list there
        self.browser = oni_browser
        HomePage(self).go_to_home_page().got_to_my_lists_page()

        self.browser.find_element_by_link_text('Edith Item 1').click()

        # On the list page Oni can see that Edith is the list owner
        self.wait_for(lambda: self.assertEqual(
            list_page.get_list_owner(),
            'edith@example.com')
        )

        # He adds a new item
        list_page.add_new_item('Hi Edith!')

        # When Edith refreshes she sees the new item

        self.browser = edith_browser
        self.browser.refresh()

        list_page.wait_for_new_item_in_list('Hi Edith!', 2)
