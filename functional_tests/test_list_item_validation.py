from .base import FunctionalTest

from unittest import skip

class ItemValidationTest(FunctionalTest):

	def get_error_element(self):
		return self.browser.find_element_by_css_selector('.has-error')
	
	def test_cannot_add_empty_list_items(self):
		# Edith goes to the homepage and tries to enter a blank item
		self.browser.get(self.server_url)
		self.get_item_input_box().send_keys('\n')

		# The homepage refreshes with an error explaining that you cannot enter blank items
		error = self.get_error_element()
		self.assertEqual(error.text, "You can't have an empty list item")

		# She tries agin with some text and it correclty adds the item
		self.get_item_input_box().send_keys('Buy Milk\n')
		self.check_for_row_in_list_table('1: Buy Milk')

		# For some reason she tries to add a blank item again
		self.get_item_input_box().send_keys('\n')

		# She receives the same error on the list page as she did on the homepage
		self.check_for_row_in_list_table('1: Buy Milk')
		error = self.get_error_element()
		self.assertEqual(error.text, "You can't have an empty list item")

		# She then gets it right and enters an item to the list
		self.get_item_input_box().send_keys('Make Tea\n')
		self.check_for_row_in_list_table('1: Buy Milk')
		self.check_for_row_in_list_table('2: Make Tea')

	def test_cannot_add_duplicate_list_items(self):
		# Edith goes to the homepage and adds an item to the list
		self.browser.get(self.server_url)
		self.get_item_input_box().send_keys('Buy Milk\n')
		self.check_for_row_in_list_table('1: Buy Milk')

		# She then accidently tries to add the same item again.
		self.get_item_input_box().send_keys('Buy Milk\n')

		# The list refreshes with an error explaining that you cannot enter blank items
		self.check_for_row_in_list_table('1: Buy Milk')
		error = self.get_error_element()
		self.assertEqual(error.text, "You can't have duplicate items")

		# The input box has been filled in with the old item
		self.assertEqual(self.get_item_input_box().get_attribute('value'), 'Buy Milk')

	def test_errors_are_cleared_on_input(self):
		#Edit starts a list that causes a validation error
		self.browser.get(self.server_url)
		self.get_item_input_box().send_keys('\n')

		error = self.get_error_element()
		self.assertTrue(error.is_displayed())

		# She starts typing in the inputbox and the error clears
		self.get_item_input_box().send_keys('a')

		# She is happy that the error message disappears
		error = self.get_error_element()
		self.assertFalse(error.is_displayed())
